const { MongoClient } = require("mongodb");
const dotenv = require("dotenv");

dotenv.config();

var _db;
module.exports = {
  connectToServer: function () {
    MongoClient.connect(process.env.MONGO_URL, function (err, client) {
      _db = client.db("mt_backend");
    });
  },
  getDb: function () {
    return _db;
  },
};
