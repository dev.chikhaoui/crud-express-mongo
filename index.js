const express = require("express");
const dotenv = require("dotenv");
const mongodbUtil = require("./utils/mongodb");
const {
  getAllUsers,
  addUser,
  getUserById,
  deleteUser,
  updateUser,
} = require("./controllers/userControllers");
const {
  paymentHandler,
  getChargeById,
  createCustomer,
  addCardToCustomer,
  findCustomerById,
  subscribeCustomer,
  getSubsrciption,
  unsubscribeCustomer,
} = require("./controllers/paymentControllers");

// Enable .env config
dotenv.config();

// Initialize express app
const app = express();
app.use(express.json());

const connectToDB = async () => {
  try {
    await mongodbUtil.connectToServer();
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

// Connect to DB
connectToDB();

// STRIPE PAIEMENT ROUTES

// add new customer
app.post("/add-customer", createCustomer);
// get customer by id
app.get("/customer/:id", findCustomerById);
// add payment card to a customer
app.post("/add-card", addCardToCustomer);
// payment handler : pay a certain amount (return a charge object if succeded)
app.post("/payment", paymentHandler);
// get a charge by id
app.get("/charge/:id", getChargeById);

// Subscribe a customer
app.post("/subscribe", subscribeCustomer);
// Get subscription by customer id
app.post("/get-subscription", getSubsrciption);
// Unsubscribe a customer
app.post("/unsubscribe", unsubscribeCustomer);

// CRUD ROUTES
//// GET ALL USERS SORTED BY NAME
app.get("/users", getAllUsers);
//// ADD USER
app.post("/add-user", addUser);
//// GET USER BY ID
app.get("/user/:id", getUserById);
//// DELET USER BY ID
app.delete("/user", deleteUser);
//// UPDATE USER BY ID
app.put("/user/:id", updateUser);

// LISTEN TO SERVER
const PORT = process.env.PORT;
app.listen(
  PORT,
  console.log(`Server running in ${process.env.NODE_ENV} on port ${PORT}`)
);
