const mongodbUtil = require("../utils/mongodb");
const { ObjectId } = require("mongodb");

// GET ALL USERS SORTED BY NAME
// API GET /users
const getAllUsers = async (req, res) => {
  const db = mongodbUtil.getDb();
  const options = {
    // sort returned documents in ascending order by name (A->Z)
    sort: { name: 1 },
  };
  db.collection("users")
    .find({}, options)
    .toArray(function (err, docs) {
      if (err) {
        console.log(err);
        throw err;
      }
      return res.status(200).json(docs);
    });
};

// GET USER BY ID
// GET /user-info/:id
const getUserById = async (req, res) => {
  const db = mongodbUtil.getDb();
  const id = ObjectId(req.params.id);
  const user = await db.collection("users").findOne({ _id: id });
  if (user) {
    return res.status(200).json({ user });
  } else {
  }
  return res.status(404).json({ message: "user not found" });
};

// ADD A NEW USER
// API POST /add-user
const addUser = async (req, res) => {
  const db = mongodbUtil.getDb();
  try {
    // add a document to user collection with request body data
    await db.collection("users").insertOne({ ...req.body });
    return res.status(201).json({ message: "user created" });
  } catch (error) {
    console.log(err);
    throw err;
  }
};

// DELETE USER BY ID
// DELETE /user || body { id : id}
const deleteUser = async (req, res) => {
  const db = mongodbUtil.getDb();
  // GET ID FROM BODY
  const id = ObjectId(req.body.id);
  const result = await db.collection("users").deleteOne({ _id: id });
  if (result.deletedCount === 1) {
    return res.status(200).json({ message: "user deleted" });
  } else {
    return res.status(404).json({ message: "user not found" });
  }
};

// UPDATE USER BY ID
// PUT /user/:id
const updateUser = async (req, res) => {
  const db = mongodbUtil.getDb();
  // GET ID FROM BODY
  const id = ObjectId(req.params.id);
  // The following example uses the $set update operator which specifies update values for document fields.
  // Get the values from body
  const updates = {
    $set: {
      ...req.body,
    },
  };
  await db.collection("users").updateOne({ _id: id }, updates);
  // Return the User object after UPDATE
  const user = await db.collection("users").findOne({ _id: id });
  res.status(201).json(user);
};

module.exports = { getAllUsers, addUser, getUserById, deleteUser, updateUser };
